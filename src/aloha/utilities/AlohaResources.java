package aloha.utilities;

public class AlohaResources {
	public final static class English implements IResourceDictionary {
		private static English instance;
		
		private English() {}

		public static English GetInstance() {
			if(instance == null) {
				Object lock = new Object();
				synchronized (lock) {
					if(instance == null) {
						instance = new English();
					}
				}
			}
			
			return instance;
		}

		@Override
		public String GetUnrecognizedCommand() {
			return "Unrecognized Command";
		}

		@Override
		public String GetScannerInputError() {
			return "There was an error while the system read the input";
		}

		@Override
		public String GetCommandWithErrors() {
			return "The command has errors!";
		}

		@Override
		public String GetDirectoryOrFileAlreadyExists() {
			return "Directory or file with the specified name already exists";
		}

		@Override
		public String GetFilenameFormatError() {
			return "The specified parameter has errors. The format of parameter must be 'name.extension'";
		}

		@Override
		public String GetDirectoryFormatError() {
			return "The specified parameter has errors. The parameter must be a directory name";
		}
		
	}
	
	private AlohaResources() {}
	
	public final static class Spanish implements IResourceDictionary {
		private static Spanish instance;
		
		private Spanish() {}
		
		public static Spanish GetInstance() {
			if(instance == null) {
				Object lock = new Object();
				synchronized (lock) {
					if(instance == null) {
						instance = new Spanish();
					}
				}
			}
			
			return instance;
		}
		
		@Override
		public String GetUnrecognizedCommand() {
			return "Comando no reconocido";
		}

		@Override
		public String GetScannerInputError() {
			return "Hubo un error mientras la aplicación leia la entrada";
		}

		@Override
		public String GetCommandWithErrors() {
			return "El comando introducido tiene errores!";
		}

		@Override
		public String GetDirectoryOrFileAlreadyExists() {
			return "El directorio o archivo especificado ya existe";
		}

		@Override
		public String GetFilenameFormatError() {
			return "El parametro especificado tiene errores. El formato del parametro debe de ser 'nombre.extension'";
		}

		@Override
		public String GetDirectoryFormatError() {
			return "El parametro especificado tiene errores. El parametro debe de ser el nombre de un directorio";
		}

	}
	
	public static IResourceDictionary GetResourceClass(String culture) {
		switch(culture) {
			case "en":
				return English.GetInstance();
			case "es":
				return Spanish.GetInstance();
			default:
				return English.GetInstance();
		}
	}
}
