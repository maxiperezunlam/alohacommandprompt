package aloha.utilities;

public interface IResourceDictionary {
	public String GetUnrecognizedCommand();
	public String GetScannerInputError();
	public String GetCommandWithErrors();
	public String GetDirectoryOrFileAlreadyExists();
	public String GetFilenameFormatError();
	public String GetDirectoryFormatError();
}
