package aloha.utilities;

import java.util.List;

import aloha.commands.CommandEnum;

public class CommandValidator {
	public static boolean IsValidCommand(CommandEnum command, List<String> params) {
		return command != null && command != CommandEnum.InvalidCommand && command.HasValidParameters(params);
	}
}
