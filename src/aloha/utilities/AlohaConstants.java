package aloha.utilities;

public final class AlohaConstants {
	public final static String PATH_TO_CONFIG_FILE = "resources/aloha.conf";
	public static final String PROMPT = "$>";
	public static final String NEWLINE = "\r\n";
	public static final String DEFAULT_CULTURE = "en";
	
	private AlohaConstants() {}
	
	public final class ConfigurationKeys {
		public final static String FILESYSTEM_KEY = "filesystem";
		public static final String CULTURE_KEY = "culture";
		
		private ConfigurationKeys() {}
	}	
}
