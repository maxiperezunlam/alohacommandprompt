package aloha.utilities;

import java.util.ArrayList;
import java.util.List;

import aloha.commands.CommandEnum;

public class CommandParser {
	private final static int COMMAND_NAME_POS = 0;
	
	public static CommandEnum Parse(String input) {
		if(input == null || input.trim().isEmpty())
			return CommandEnum.InvalidCommand;
		
		String[] commandParts = input.split(" ");
		
		if(commandParts != null) {			
			switch(commandParts[COMMAND_NAME_POS]) {
				case "quit":
					return CommandEnum.Quit;
				case "pwd":
					return CommandEnum.CurrentDirectory;
				case "ls":
					return CommandEnum.ListFiles;
				case "mkdir":
					return CommandEnum.MakeDirectory;
				case "cd":
					return CommandEnum.ChangeDirectory;
				case "touch":
					return CommandEnum.CreateFile;
				default: 
					return CommandEnum.InvalidCommand;
			}
		}
		
		return CommandEnum.InvalidCommand;
	}

	public static ArrayList<String> ParseParameters(String input) {
		if(input == null || input.trim().isEmpty()) 
			return null;
		
		String[] commandParts = input.split(" ");
		
		if(commandParts == null || commandParts.length < 1)
			return null;

		ArrayList<String> parameters = new ArrayList<String>();

		for(int i = 1; i < commandParts.length; i++)
			parameters.add(commandParts[i].trim());
		
		return parameters; 
	}
}
