package aloha.utilities;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Optional;
import java.util.Scanner;

public class AlohaConfiguration {
	private static HashMap<String, String> conf;
	private static AlohaConfiguration instance;
	private static String CONFIGURATION_SEPARATOR="=";
	
	private AlohaConfiguration() {
		conf = new HashMap<>();
		ReadConfigurationFile();
	}
	
	private void ReadConfigurationFile() {
		File file = new File(AlohaConstants.PATH_TO_CONFIG_FILE);
		String line = null;
		
		try(Scanner scanner = new Scanner(file)) {
			while(scanner.hasNextLine()) {
				line = scanner.nextLine().trim();
				String[] keyAndValue = line.split(CONFIGURATION_SEPARATOR);
				
				if(keyAndValue == null || keyAndValue.length < 2)
					continue;
				
				conf.put(keyAndValue[0], keyAndValue[1]);
			}
		} catch(IOException ex) {
			System.err.println(ex.getMessage());
		}	
	}

	public static AlohaConfiguration GetInstance() {		
		if(instance == null) {
			Object lock = new Object();

			synchronized(lock) {
				if(instance == null) {
					instance = new AlohaConfiguration();
				}
			}			
		}
		
		return instance;
	}
	
	public Optional<String> Get(String key) {
		if(conf.containsKey(key))
			return Optional.of(conf.get(key));
		else
			return Optional.empty();
	}

	public String GetCulture() {
		Optional<String> culture = Get(AlohaConstants.ConfigurationKeys.CULTURE_KEY);
		return culture.isPresent() ? culture.get() : AlohaConstants.DEFAULT_CULTURE;
	}
}
