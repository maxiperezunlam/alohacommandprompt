package aloha.filesystem;

import java.security.InvalidParameterException;

public interface IFileSystem {
	public boolean ExistsFile(String filePath);
	public boolean ExistsDirectory(String filePath);
	public void AddEntry(String filePath, FileType type);
	public boolean IsFile(String filename);
	public void ChangeCurrentDirectory(String filePath) throws InvalidParameterException;
	public void PrintCurrentDirectory();
	public void ListFilesInCurrentDirectory(boolean isRecursive);
}
