package aloha.filesystem;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * 	Hubiese sido mejor guardar las entradas en un arbol para representar el filesystem.
	Operaciones como por ejemplo movernos de directorio en directorio involucraria simplemente
	recorrer el �rbol. Por otro lado el punto seria simplemente una referencia a mantenernos en el
	nodo actuald el �rbol y los dos puntos una referencia a volver un nodo hacia atras.
	
	No obstante, por ello implementamos una interfaz generica de filesystem para que en caso
	de querer modificar la implementacion simplemente podamos implementarla en una clase nueva
	y por fuera el sistema sea independiente de los detalles de implementacion.
 * */
public class VirtualFileSystem implements IFileSystem {
	private static VirtualFileSystem instance;
	private static String PATH_SEPARATOR = "/";
	private static String DOT = ".";
	private static String DOUBLE_DOT = "..";
	
	private final HashMap<String, FileType> cache;
	private String currentDirectory;
	
	private VirtualFileSystem() {
		cache = new HashMap<>();
		cache.put(VirtualFileSystem.PATH_SEPARATOR, FileType.Directory);
		currentDirectory = VirtualFileSystem.PATH_SEPARATOR;
	}
	
	public static VirtualFileSystem GetInstance() {		
		if(instance == null) {
			Object lock = new Object();

			synchronized(lock) {
				if(instance == null) {
					instance = new VirtualFileSystem();
				}
			}			
		}
		
		return instance;
	}
	
	public boolean ExistsFile(String filePath) {
		FileType type = cache.getOrDefault(IsAbsoluteFilePath(filePath) ? filePath : GetAbsoluteFilePath(filePath), null);
		
		return type != null && type == FileType.File;
	}
	
	public boolean ExistsDirectory(String filePath) {
		FileType type = cache.getOrDefault(IsAbsoluteFilePath(filePath) ? filePath : GetAbsoluteFilePath(filePath), null);
		
		return type != null && type == FileType.Directory;
	}
	
	public void AddEntry(String filePath, FileType type) {
			cache.putIfAbsent(IsAbsoluteFilePath(filePath) ? filePath : GetAbsoluteFilePath(filePath), type);
	}
		
	public boolean IsFile(String filename) {
		if(filename == null || filename.isEmpty())
			return false;
		
		String[] filePaths = filename.split("/");
		String[] fileParts = null;
		
		//No vamos a contemplar archivos ocultos tipo unix, etcetera.		
		if(filePaths != null && filePaths.length > 1) {
			fileParts = filePaths[filePaths.length - 1].split("\\.");			
		} else {
			fileParts = filename.split("\\.");
		}
		
		return fileParts.length > 1;
	}

	public void ChangeCurrentDirectory(String filePath) throws InvalidParameterException {
		String[] directories = filePath.split(VirtualFileSystem.PATH_SEPARATOR);
		StringBuilder appendDir = new StringBuilder("");
		boolean firstAppend = !IsRootPath(this.currentDirectory);

		for(int i = 0; i < directories.length; i++) {
			if(IsADot(directories[i]))
				continue;
			
			if(IsTwoDots(directories[i])) {
				RemoveLastDirectory();
				continue;
			}
						
			if(!IsFile(directories[i])) {
				String pathToAnalize = i == 0 && directories[i].equals("") ? VirtualFileSystem.PATH_SEPARATOR : appendDir.toString() + directories[i]; 
	
				if(ExistsDirectory(pathToAnalize)) {
					if(firstAppend) {
						firstAppend = false;
						appendDir.append(VirtualFileSystem.PATH_SEPARATOR);
					}

					appendDir.append(directories[i]);				
				} else {
					throw new InvalidParameterException("The specified path does not exist");
				}
			}			
		}
		
		if(IsAbsoluteFilePath(filePath)) {
			currentDirectory = appendDir.toString();
		} else {			
			currentDirectory += appendDir.toString();
		}
	}	
	
	public void PrintCurrentDirectory() {
		System.out.println(this.currentDirectory);
	}

	//Para realizar una busqueda eficiente habria dos alternativas:
	//En el caso de buscar strings tal cual es esta implementacion
	//deberiamos de utilizar un Trie. De esta forma podriamos buscar utilizando un prefijo.
	//En el caso de un filesystem verdadero, lo que hariamos seria 
	//imprimir todos los nodos hijos del nodo apuntado por un puntero
	//que mantiene almacenada la posicion actual donde se encuentra parado el usuario dentro
	//del filesystem. Por cuestiones de tiempo y simplicidad al tratarse de un ejercicio
	//vamos a implementarlo utilizando el enfoque actual (HashMap que almacena strings).
	public void ListFilesInCurrentDirectory(boolean isRecursive) {
		Set<String> entries = cache.keySet();
		
		ListFilesInDirectory(isRecursive, entries, this.currentDirectory);
	}
	
	private void ListFilesInDirectory(boolean isRecursive, Set<String> entries, String basePath) {
		long separatorsInBasePath = 0;
		
		String strPattern = "^" + basePath;
		boolean isRootPath = IsRootPath(basePath);
		
		if(!isRootPath) {
			strPattern += VirtualFileSystem.PATH_SEPARATOR;
			separatorsInBasePath = CountSeparatorsInPath(basePath + VirtualFileSystem.PATH_SEPARATOR);
		}
		
		strPattern = strPattern.replaceAll("/", Matcher.quoteReplacement("\\/"));
		Pattern basePathPattern = Pattern.compile(strPattern);		
		
		for(String entry : entries) {
			//No imprimimos si la entrada analizada es el path que estamos parados o desde donde venimos
			//Analizamos si tiene el path como prefijo
			//Comparamos la cantidad de separadores para evitar imprimir recursivamente las entradas
			if(!entry.equals(basePath) && basePathPattern.matcher(entry).find()) {
				long separatorsInEntry = CountSeparatorsInPath(entry);
				if((isRootPath && separatorsInEntry == 1) || separatorsInEntry == separatorsInBasePath) {
					System.out.println(String.format("%-1s %-100s", cache.get(entry).ToSym(), entry));

					if(isRecursive && cache.get(entry) == FileType.Directory)
						ListFilesInDirectory(isRecursive, entries, entry);
				}
			}
		}		
	}
	
	private long CountSeparatorsInPath(String filePath) {
		return filePath.chars().filter(ch -> ch == '/').count();
	}
	
	private boolean IsRootPath(String filePath) {
		return filePath.equals(VirtualFileSystem.PATH_SEPARATOR);
	}
	

	private boolean IsADot(String filename) {
		if(filename == null || filename.isEmpty())
			return false;
		
		return filename.equals(VirtualFileSystem.DOT);
	}
	
	private boolean IsTwoDots(String filename) {
		if(filename == null || filename.isEmpty())
			return false;
		
		return filename.equals(VirtualFileSystem.DOUBLE_DOT);
	}
	
	private void RemoveLastDirectory() {
		String[] directories = this.currentDirectory.split(VirtualFileSystem.PATH_SEPARATOR);
		
		StringBuilder newCurrentDirectory = new StringBuilder(VirtualFileSystem.PATH_SEPARATOR);
		
		for(int i = 0; i < directories.length - 1; i++) {
			newCurrentDirectory.append(directories[i]);
			
			if(i != directories.length - 2 && i != 0)
				newCurrentDirectory.append(VirtualFileSystem.PATH_SEPARATOR);
		}
		
		this.currentDirectory = newCurrentDirectory.toString();
	}
	
	private String GetAbsoluteFilePath(String filePath) {
		String separator = "";
		if(!IsRootPath(this.currentDirectory))
			separator = VirtualFileSystem.PATH_SEPARATOR;
		
		return this.currentDirectory + separator + filePath;
	}
	
	private boolean IsAbsoluteFilePath(String filePath) {
		return filePath.indexOf(VirtualFileSystem.PATH_SEPARATOR) == 0;
	}
}
