package aloha.filesystem;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;

import aloha.utilities.AlohaConfiguration;
import aloha.utilities.AlohaConstants;

public final class FileSystemFactory {
	private static IFileSystem instance;
	
	private FileSystemFactory() {}
	
	public static IFileSystem GetFileSystemFromConfigurationFile() {		
		if(instance == null) {
			Object lock = new Object();

			synchronized(lock) {
				if(instance == null) {
					AlohaConfiguration configuration = AlohaConfiguration.GetInstance();
					Optional<String> fileSystem = configuration.Get(AlohaConstants.ConfigurationKeys.FILESYSTEM_KEY);		
					
					if(fileSystem.isPresent()) {
						try {
							Method fileSystemGetInstanceMethod = Class.forName(fileSystem.get()).getDeclaredMethod("GetInstance");
							instance = (IFileSystem) fileSystemGetInstanceMethod.invoke(null);
						} catch (NoSuchMethodException | SecurityException | ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
							System.err.println(ex.getMessage());
							
							//Abort the program
							System.exit(0);
						}						
					}
				}
			}
		}
		
		return instance;
	}
}
