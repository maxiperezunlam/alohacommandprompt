package aloha.filesystem;

public enum FileType {
	Directory {
		@Override
		public String ToSym() {
			return "d";
		}
	}, File {
		@Override
		public String ToSym() {
			return "f";
		}
	};

	public abstract String ToSym();
}
