package aloha.commands;

import java.security.InvalidParameterException;
import java.util.List;

import aloha.filesystem.FileSystemFactory;
import aloha.filesystem.IFileSystem;
import aloha.utilities.IResourceDictionary;

public class ChangeDirectoryCommand extends IShellCommand {

	public ChangeDirectoryCommand(List<String> params, IResourceDictionary resDictionary) {
		super(params, resDictionary);
	}

	@Override
	protected boolean ValidateParameters(List<String> params) {
		if(params == null || params.size() < 1)
			return false;
		
		String param = params.get(0);
		
		if(param == null)
			return false;
		
		param = param.trim();
		
		if(param.length() < 1)
			return false;

		IFileSystem vfs = FileSystemFactory.GetFileSystemFromConfigurationFile();
		
		if(vfs.IsFile(param))
			return false;
		
		return true;
	}

	@Override
	protected void RunCommand(List<String> params) {
		try {
			IFileSystem vfs = FileSystemFactory.GetFileSystemFromConfigurationFile();
			vfs.ChangeCurrentDirectory(params.get(0));			
		} catch(InvalidParameterException ex) {
			System.out.println(ex.getMessage());
		}
	}

	@Override
	protected void ShowErrorMessage() {
		System.out.println(this.resourceDictionary.GetDirectoryFormatError());
	}
}
