package aloha.commands;

import java.util.List;

import aloha.filesystem.FileSystemFactory;
import aloha.filesystem.IFileSystem;
import aloha.utilities.IResourceDictionary;

public class CurrentDirectoryCommand extends IShellCommand {

	public CurrentDirectoryCommand(List<String> params, IResourceDictionary resDictionary) {
		super(params, resDictionary);
	}

	@Override
	protected boolean ValidateParameters(List<String> params) {
		return true;
	}

	@Override
	protected void RunCommand(List<String> params) {
		IFileSystem vfs = FileSystemFactory.GetFileSystemFromConfigurationFile();
		vfs.PrintCurrentDirectory();
	}

}
