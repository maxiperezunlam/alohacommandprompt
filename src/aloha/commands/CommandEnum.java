package aloha.commands;

import java.util.ArrayList;
import java.util.List;

import aloha.utilities.AlohaConfiguration;
import aloha.utilities.AlohaResources;
import aloha.utilities.IResourceDictionary;

public enum CommandEnum {
	ChangeDirectory {
		@Override
		public boolean HasValidParameters(List<String> params) {
			if(params == null || params.isEmpty())
				return true;
			
			String param = params.get(0);
			return param != null && !param.trim().isEmpty();		
		}

		@Override
		public IShellCommand GetCommand(ArrayList<String> parameters) {
			return new ChangeDirectoryCommand(parameters, GetResourceDictionary());
		}
	}, CreateFile {
		@Override
		public boolean HasValidParameters(List<String> params) {
			if(params == null || params.isEmpty())
				return true;
			
			String param = params.get(0);
			return param != null && !param.trim().isEmpty() && param.trim().length() <= 100;		
		}
		
		@Override
		public IShellCommand GetCommand(ArrayList<String> parameters) {
			return new CreateFileCommand(parameters, GetResourceDictionary());
		}
	}, CurrentDirectory {
		//Ignoramos los parametros que nos pasen
		@Override
		public boolean HasValidParameters(List<String> params) {
			return true;
		}
		
		@Override
		public IShellCommand GetCommand(ArrayList<String> parameters) {
			return new CurrentDirectoryCommand(parameters, GetResourceDictionary());
		}
	}, ListFiles {
		@Override
		public boolean HasValidParameters(List<String> params) {
			if(params == null || params.isEmpty())
				return true;
			
			String param = params.get(0);
			return param != null && param.trim().equals("-r");			
		}
		
		@Override
		public IShellCommand GetCommand(ArrayList<String> parameters) {
			return new ListCommand(parameters, GetResourceDictionary());
		}
	}, MakeDirectory {
		@Override
		public boolean HasValidParameters(List<String> params) {
			if(params == null || params.isEmpty())
				return true;
			
			String param = params.get(0);
			return param != null && !param.trim().isEmpty();		
		}
		
		@Override
		public IShellCommand GetCommand(ArrayList<String> parameters) {
			return new MakeDirectoryCommand(parameters, GetResourceDictionary());
		}
	}, Quit {
		//Ignoramos los parametros que se nos pase
		@Override
		public boolean HasValidParameters(List<String> params) {
			return true;
		}
		
		@Override
		public IShellCommand GetCommand(ArrayList<String> parameters) {
			return new QuitCommand(parameters, GetResourceDictionary());
		}
	}, InvalidCommand {
		//Siempre los comandos invalidos los validamos como incorrectos
		@Override
		public boolean HasValidParameters(List<String> params) {
			return false;
		}
		
		@Override
		public IShellCommand GetCommand(ArrayList<String> parameters) {
			return new NullCommand(parameters, null);
		}
	};
	
	public abstract boolean HasValidParameters(List<String> params);

	protected IResourceDictionary GetResourceDictionary() {
		AlohaConfiguration configuration = AlohaConfiguration.GetInstance();
		return AlohaResources.GetResourceClass(configuration.GetCulture());
	}

	public abstract IShellCommand GetCommand(ArrayList<String> parameters);
}
