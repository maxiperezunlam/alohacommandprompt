package aloha.commands;

import java.util.List;

import aloha.filesystem.FileSystemFactory;
import aloha.filesystem.FileType;
import aloha.filesystem.IFileSystem;
import aloha.utilities.IResourceDictionary;

public class MakeDirectoryCommand extends IShellCommand {
	public MakeDirectoryCommand(List<String> params, IResourceDictionary resDictionary) {
		super(params, resDictionary);
	}

	@Override
	protected boolean ValidateParameters(List<String> params) {
		if(params == null || params.size() < 1)
			return false;
		
		String param = params.get(0);
		
		if(param == null)
			return false;
		
		param = param.trim();
		
		if(param.length() < 1)
			return false;
		
		return true;
	}

	@Override
	protected void RunCommand(List<String> params) {
		IFileSystem vfs = FileSystemFactory.GetFileSystemFromConfigurationFile();
		
		if(vfs.ExistsDirectory(params.get(0)) || vfs.ExistsFile(params.get(0))) {
			System.out.println(this.resourceDictionary.GetDirectoryOrFileAlreadyExists());
		} else {
			vfs.AddEntry(params.get(0), FileType.Directory);
		}		
	}

}
