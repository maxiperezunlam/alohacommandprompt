package aloha.commands;

import java.util.List;

import aloha.utilities.IResourceDictionary;

/*
 * Null object command
 * */
public class NullCommand extends IShellCommand {

	public NullCommand(List<String> params, IResourceDictionary resDictionary) {
		super(params, resDictionary);
	}
	
	@Override
	protected boolean ValidateParameters(List<String> params) {
		return true;
	}

	@Override
	protected void RunCommand(List<String> params) {
		
	}
	
	@Override
	protected void ShowErrorMessage() {
		
	}
}
