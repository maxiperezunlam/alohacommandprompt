package aloha.commands;

import java.util.List;

import aloha.utilities.IResourceDictionary;

public class QuitCommand extends IShellCommand {

	public QuitCommand(List<String> params, IResourceDictionary resDictionary) {
		super(params, resDictionary);
	}

	@Override
	protected boolean ValidateParameters(List<String> params) {
		return true;
	}

	@Override
	protected void RunCommand(List<String> params) {

	}

}
