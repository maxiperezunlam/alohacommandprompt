package aloha.commands;

import java.util.List;

import aloha.utilities.IResourceDictionary;

public abstract class IShellCommand {
	protected IResourceDictionary resourceDictionary;
	
	@SuppressWarnings("rawtypes")
	private final List<String> params;
	
	@SuppressWarnings("rawtypes")
	public IShellCommand(List<String> params, IResourceDictionary resDictionary) {
		this.params = params;
		this.resourceDictionary = resDictionary;
	}
	
	@SuppressWarnings("rawtypes")
	public void Execute() {		
		if(ValidateParameters(this.params)) {
			RunCommand(this.params);
		} else {
			ShowErrorMessage();
		}
	}

	@SuppressWarnings("rawtypes")
	protected abstract boolean ValidateParameters(List<String> params);
	@SuppressWarnings("rawtypes")
	protected abstract void RunCommand(List<String> params);
	
	protected void ShowErrorMessage() {
		System.out.println(resourceDictionary.GetCommandWithErrors());	
	}
}
