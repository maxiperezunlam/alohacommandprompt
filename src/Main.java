import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Scanner;

import aloha.commands.CommandEnum;
import aloha.utilities.AlohaConfiguration;
import aloha.utilities.AlohaConstants;
import aloha.utilities.AlohaResources;
import aloha.utilities.CommandParser;
import aloha.utilities.CommandValidator;
import aloha.utilities.IResourceDictionary;

public class Main {

	public static void main(String[] args) {
		String input = null;
		
		CommandEnum selectedCommand = CommandEnum.InvalidCommand;
		AlohaConfiguration configuration = AlohaConfiguration.GetInstance();
		IResourceDictionary resourceDictionary = AlohaResources.GetResourceClass(configuration.GetCulture());
				
		try(Scanner scanner = new Scanner(System.in)) {
			do {
				System.out.print(AlohaConstants.PROMPT);
				input = scanner.nextLine();
				
				if(input == null || input.isEmpty()) {
					System.out.print(AlohaConstants.NEWLINE);
					continue;
				}
				
				selectedCommand = CommandParser.Parse(input);
				ArrayList<String> parameters = CommandParser.ParseParameters(input);
				
				if(!CommandValidator.IsValidCommand(selectedCommand, parameters)) {
					System.out.println(resourceDictionary.GetUnrecognizedCommand());
				} else {
					selectedCommand.GetCommand(parameters).Execute();
				}
			} while(selectedCommand != CommandEnum.Quit);			
		} catch(NoSuchElementException ex) {
			System.err.println(resourceDictionary.GetScannerInputError());
			System.err.println(ex.getMessage());
		}
	}

}
